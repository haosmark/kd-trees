import java.util.ArrayList;

public class PointSET {
    private SET<Point2D> tree;

    /**
     * construct an empty set of points
     */
    public PointSET() {
        tree = new SET<Point2D>();
    }

    /**
     * @return true if the set is empty
     */
    public boolean isEmpty() {
        return tree.isEmpty();
    }

    /**
     * @return number of points in the set
     */
    public int size() {
        return tree.size();
    }

    /**
     * add the point to the set (if it is not already in the set)
     * @param p
     */
    public void insert(Point2D p) {
        if (p == null) {
            throw new java.lang.NullPointerException();
        }
        tree.add(p);
    }

    /**
     * @param p
     * @return true if the set contain point p
     */
    public boolean contains(Point2D p) {
        if (p == null) {
            throw new java.lang.NullPointerException();
        }
        return tree.contains(p);
    }

    /**
     * draw all points to standard draw
     */
    public void draw() {
        for (Point2D point : tree) {
            StdDraw.point(point.x(), point.y());
        }
    }

    /**
     * @param rect
     * @return all points that are inside the rectangle
     */
    public Iterable<Point2D> range(RectHV rect) {
        if (rect == null) {
            throw new java.lang.NullPointerException();
        }

        ArrayList<Point2D> points = new ArrayList<Point2D>();
        for (Point2D point : tree) {
            if (rect.contains(point)) {
                points.add(point);
            }
        }
        return points;
    }

    /**
     *
     * @param p
     * @return a nearest neighbor in the set to point p; null if the set is empty
     */
    public Point2D nearest(Point2D p) {
        if (p == null) {
            throw new java.lang.NullPointerException();
        }
        Point2D closest = null;
        for (Point2D point : tree) {
            if (closest == null || point.distanceSquaredTo(p) < closest.distanceSquaredTo(p)) {
                closest = point;
            }
        }
        return closest;
    }

    public static void main(String[] args) {}           // unit testing of the methods (optional)
}
