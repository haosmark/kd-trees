import java.util.ArrayList;

public class KdTree {
    private int size;
    private Node root;
    private final boolean vertical = true;
    private final boolean horizontal = false;

    private class Node {
        private Point2D point;
        private Node left;
        private Node right;
        private boolean vertical;
        public Node(Point2D point, boolean vertical) {
            this.point = point;
            this.vertical = vertical;
        }
        public boolean isVertical() {
            return vertical;
        }
        public boolean isHorizontal() {
            return !vertical;
        }
    }
    /**
     * construct an empty set of points
     */
    public KdTree() {
        size = 0;
    }

    /**
     * @return true if the set is empty
     */
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * @return number of points in the set
     */
    public int size() {
        return size;
    }

    /**
     * add the point to the set (if it is not already in the set)
     * @param p - point to insert
     */
    public void insert(Point2D p) {
        if (p == null) {
            throw new java.lang.NullPointerException();
        }
        if (size == 0) {
            root = new Node(p, vertical);
        } else {
            insert(root, p);
        }
        size++;
    }

    private void insert(Node node, Point2D p) {
        if (node == null) {
            return;
        }
        if (node.point.equals(p)) {
            size--; // duplicate point size fix
            return;
        }

        if (node.isVertical()) {
            // if the point to be inserted is to the left of the node
            if (p.x() < node.point.x()) {
                if (node.left == null) {
                    node.left = new Node(p, horizontal);
                } else {
                    insert(node.left, p);  // go left
                }
            // if the point to be inserted is to the right (or equal) of the node
            } else {
                if (node.right == null) {
                    node.right = new Node(p, horizontal);
                } else {
                    insert(node.right, p);  // go right
                }
            }
        } else if (node.isHorizontal()) {
            // if the point to be inserted is lower than then the node
            if (p.y() < node.point.y()) {
                if (node.left == null) {
                    node.left = new Node(p, vertical);
                } else {
                    insert(node.left, p);  // go left
                }
            // if the point to be inserted is higher (or equal) than then the node
            } else {
                if (node.right == null) {
                    node.right = new Node(p, vertical);
                } else {
                    insert(node.right, p);  // go right
                }
            }
        }
    }

    /**
     * @param p - point to search
     * @return true if the set contain point p
     */
    public boolean contains(Point2D p) {
        if (p == null) {
            throw new java.lang.NullPointerException();
        }
        Node node = root;
        while (node != null && !node.point.equals(p)) {
            // sink
            if (node.isVertical()) {
                if (p.x() < node.point.x()) {
                    node = node.left;  // go left
                } else {
                    node = node.right; // go right
                }
            } else if (node.isHorizontal()) {
                if (p.y() < node.point.y()) {
                    node = node.left;  // go left
                } else {
                    node = node.right; // go right
                }
            }
        }
        return (node == null) ? false : true;
    }

    /**
     * draw all points to standard draw
     */
    public void draw() {
        draw(root);
        StdDraw.show();
    }
    private void draw(Node node) {
        if (node == null) {
            return;
        }
        StdDraw.setPenColor(StdDraw.BLACK);
        StdDraw.setPenRadius(0.01);
        StdDraw.point(node.point.x(), node.point.y());
        StdDraw.setPenRadius();
        if (node.isVertical()) {
            StdDraw.setPenColor(StdDraw.RED);
            StdDraw.line(node.point.x(), 0, node.point.x(), 1);
        } else {
            StdDraw.setPenColor(StdDraw.BLUE);
            StdDraw.line(0, node.point.y(), 1, node.point.y());
        }
        draw(node.left);
        draw(node.right);
    }

    /**
     * @param rect
     * @return all points that are inside the rectangle
     */
    public Iterable<Point2D> range(RectHV rect) {
        if (rect == null) {
            throw new java.lang.NullPointerException();
        }
        ArrayList points = new ArrayList();
        traverse(root, rect, points);
        return points;
    }

    private void traverse(Node node, RectHV rect, ArrayList points) {
        if (node == null) {
            return;
        }
        if (inRectangle(node.point, rect)) {
            points.add(node.point);
        }

        if (node.isVertical()) {
            // is rectangle to the left?
            if (rect.xmax() < node.point.x()) {
                traverse(node.left, rect, points);
            // is rectangle to the right?
            } else if (rect.xmin() > node.point.x()) {
                traverse(node.right, rect, points);
            } else {
                traverse(node.left, rect, points);
                traverse(node.right, rect, points);
            }
        } else if (node.isHorizontal()) {
            if (rect.ymax() < node.point.y()) {
                traverse(node.left, rect, points);
            } else if (rect.ymin() > node.point.y()) {
                traverse(node.right, rect, points);
            } else {
                traverse(node.left, rect, points);
                traverse(node.right, rect, points);
            }
        }
    }

    private boolean inRectangle(Point2D p, RectHV rect) {
        if (p.x() <= rect.xmax() && p.x() >= rect.xmin() && p.y() <= rect.ymax() && p.y() >= rect.ymin()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @param
     * @return a nearest neighbor in the set to point p; null if the set is empty
     */
    public Point2D nearest(Point2D p) {
        if (p == null) {
            throw new java.lang.NullPointerException();
        }
        return traverse(root, p, root.point);
    }

    private Point2D traverse(Node node, Point2D p, Point2D closest) {
        if (node == null) {
            return closest;
        }
        if (node.point.distanceSquaredTo(p) < closest.distanceSquaredTo(p)) {
            closest = node.point;
        }

        if (node.isVertical()) {
            if (node.right != null && node.right.point.x() == node.point.x()) {
                closest = traverse(node.left, p, closest);
                closest = traverse(node.right, p, closest);
            } else {
                if (p.x() < node.point.x()) {
                    closest = traverse(node.left, p, closest);
                } else {
                    closest = traverse(node.right, p, closest);
                }
            }
        } else if (node.isHorizontal()) {
            if (node.right != null && node.right.point.y() == node.point.y()) {
                closest = traverse(node.left, p, closest);
                closest = traverse(node.right, p, closest);
            } else {
                if (p.y() < node.point.y()) {
                    closest = traverse(node.left, p, closest);
                } else {
                    closest = traverse(node.right, p, closest);
                }
            }
        }
        return closest;
    }

    public static void main(String[] args) {}           // unit testing of the methods (optional)
}
