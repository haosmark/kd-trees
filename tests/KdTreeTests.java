import org.junit.*;
import org.junit.Test;

import java.util.ArrayList;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.*;

public class KdTreeTests {
    @org.junit.Test
    public void testInsert() {
        KdTree tree = new KdTree();

        tree.insert(new Point2D(0.7, 0.2));
        tree.insert(new Point2D(0.5, 0.4));
        tree.insert(new Point2D(0.9, 0.6));
        tree.insert(new Point2D(0.2, 0.3));
        tree.insert(new Point2D(0.4, 0.7));
        assertThat(tree.size(), is(5));
    }
    @org.junit.Test
    public void testContains() {
        KdTree tree = new KdTree();
        tree.insert(new Point2D(0.9, 0.5));
        tree.insert(new Point2D(0.2, 0.5));
        tree.insert(new Point2D(0.3, 0.5));
        tree.insert(new Point2D(0.4, 0.5));
        tree.insert(new Point2D(0.1, 0.5));
        tree.insert(new Point2D(0.6, 0.5));
        tree.insert(new Point2D(0.5, 0.5));
        tree.insert(new Point2D(0.7, 0.5));

        assertThat(tree.contains(new Point2D(0.1, 0.5)), is(true));
    }

    @Test
    public void testRange() {
        KdTree tree = new KdTree();
        tree.insert(new Point2D(0.7, 0.2));
        tree.insert(new Point2D(0.5, 0.4));
        tree.insert(new Point2D(0.9, 0.6));
        tree.insert(new Point2D(0.2, 0.3));
        tree.insert(new Point2D(0.4, 0.7));

        // test rectangle that covers only one point
        RectHV rect = new RectHV(0.85, 0.55, 0.95, 0.65);
        ArrayList points = (ArrayList) tree.range(rect);
        assertThat(points.size(), is(1));
    }

    @Test
    public void testNearestForHorizontalLine() {
        KdTree tree = new KdTree();
        tree.insert(new Point2D(0.9, 0.5));
        tree.insert(new Point2D(0.2, 0.5));
        tree.insert(new Point2D(0.3, 0.5));
        tree.insert(new Point2D(0.4, 0.5));
        tree.insert(new Point2D(0.1, 0.5));
        tree.insert(new Point2D(0.6, 0.5));
        tree.insert(new Point2D(0.5, 0.5));
        tree.insert(new Point2D(0.7, 0.5));

        Point2D p = new Point2D(0.7, 0.6);
        assertThat(tree.nearest(p), is(new Point2D(0.7, 0.5)));
        p = new Point2D(0.7, 0.4);
        assertThat(tree.nearest(p), is(new Point2D(0.7, 0.5)));
//        tree.insert(new Point2D(0.7, 0.2));
//        tree.insert(new Point2D(0.5, 0.4));
//        tree.insert(new Point2D(0.9, 0.6));
//        tree.insert(new Point2D(0.2, 0.3));
//        tree.insert(new Point2D(0.4, 0.7));
//
//        Point2D p = new Point2D(0.1, 0.3);
//        assertThat(tree.nearest(p), is(new Point2D(0.2, 0.3)));
//
//        p = new Point2D(1, 0.6);
//        assertThat(tree.nearest(p), is(new Point2D(0.9, 0.6)));
    }
    @Test
    public void testNearestForVerticalLine() {
        KdTree tree = new KdTree();
        tree.insert(new Point2D(0.3, 0.9));
        tree.insert(new Point2D(0.3, 0.1));
        tree.insert(new Point2D(0.3, 0.2));
        tree.insert(new Point2D(0.3, 0.8));
        tree.insert(new Point2D(0.3, 0.4));
        tree.insert(new Point2D(0.3, 0.7));
        tree.insert(new Point2D(0.3, 0.5));

        Point2D p = new Point2D(0.2, 0.4);
        assertThat(tree.nearest(p), is(new Point2D(0.3, 0.4)));
    }
}
